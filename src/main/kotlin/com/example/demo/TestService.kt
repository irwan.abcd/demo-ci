package com.example.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestService {

    @GetMapping("/test")
    fun printTest(): String {
        return "Testing Hello world"
    }
}